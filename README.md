# Ajaib Web Engineering Test

## About Project
This project created by [Harun Arrasyid Hasibuan](https://harunhsb.com/) as a submission of Test for Ajaib Web Engineer Position

## Library

When I build this project, I use some library, here they are :
* [Chakra UI](https://chakra-ui.com/), as a component library
* [Fontawesome](https://fontawesome.com/), for some icon
* [React Table](https://tanstack.com/table/v8/?from=reactTableV7&original=https://react-table-v7.tanstack.com/)
* React Context, as state management

## Installation

After you clone this project, please install all library first :

```
npm run install
```
After that, you can run the project :
```
npm run start
```
If you want to do the test :
```
npm run test
```
## Contact

* Web : [harunhsb.com](https://harunhsb.com)
* LinkedIn : [https://www.linkedin.com/in/harun-arrasyid-hasibuan-38885a134/](https://www.linkedin.com/in/harun-arrasyid-hasibuan-38885a134/)