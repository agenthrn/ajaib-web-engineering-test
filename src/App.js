import React from "react";
import { ChakraProvider } from "@chakra-ui/react";
import DataState from "./context/DataState";
import UserLists from "./pages/UserListsPage";

const App = () => {
  return (
    <DataState>
      <ChakraProvider>
        <UserLists />
      </ChakraProvider>
    </DataState>
  );
};

export default App;
