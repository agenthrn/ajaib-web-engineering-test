import React, { useContext } from "react";
import { IconButton, Button, Box } from "@chakra-ui/react";
import { ChevronLeftIcon, ChevronRightIcon } from "@chakra-ui/icons";
import DataContext from "./DataContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisH } from "@fortawesome/free-solid-svg-icons";

const Pagination = () => {
  const { lastPage, currentPage, selectedGender, keyword, getData } =
    useContext(DataContext);

  const render = [];

  if (currentPage - 1 >= 0) {
    const prevPage = currentPage - 1;
    render.push(
      <IconButton
        icon={<ChevronLeftIcon />}
        value={prevPage}
        onClick={() => getData(prevPage, selectedGender, keyword)}
        key={`prev-page-${prevPage}`}
      />
    );
  }

  let startIdx;
  let endIdx;
  if (currentPage - 1 >= 0) {
    startIdx = currentPage - 1;
    endIdx = startIdx + 5;
  } else {
    startIdx = 0;
    endIdx = 5;
  }
  if (currentPage + 3 >= lastPage) {
    startIdx = lastPage - 5;
    endIdx = lastPage;
  }

  for (let idx = startIdx; idx < endIdx; idx++) {
    const offset = idx + 1;
    render.push(
      <Button
        key={`page-${offset}`}
        onClick={() => getData(offset, selectedGender, keyword)}
        value={idx}
      >
        {offset}
      </Button>
    );
  }

  if (endIdx < lastPage) {
    const offset = lastPage - 1;
    render.push(
      <React.Fragment key={`last-page-${lastPage}`}>
        <FontAwesomeIcon icon={faEllipsisH} color="gray" />
        <Button
          onClick={() => getData(offset, selectedGender, keyword)}
          value={offset}
        >
          {lastPage}
        </Button>
      </React.Fragment>
    );
  }

  if (currentPage + 1 < lastPage) {
    const nextPage = currentPage + 1;
    render.push(
      <IconButton
        icon={<ChevronRightIcon />}
        value={nextPage}
        onClick={() => getData(currentPage + 1, selectedGender, keyword)}
        key={`next-page-${nextPage}`}
      />
    );
  }
  return (
    <Box float="right" marginTop="20px">
      {render}
    </Box>
  );
};

export default Pagination;
