const DataReducer = (state, action) => {
  const { type, payload } = action;
  switch (type) {
    case "SET_LOADING":
      return { ...state, isLoading: payload };
    case "SET_DATA":
      return {
        ...state,
        isLoading: false,
        users: payload.response.results,
        selectedGender: payload.selectedGender,
        currentPage: payload.response.info.page,
      };
    case "RESET_DATA":
      return (state = payload);
    case "SET_KEYWORD":
      return {
        ...state,
        keyword: payload,
      };
    case "SET_SORT":
      return {
        ...state,
        selectedSortOrder: payload.sortOrder,
        selectedSortBy: payload.sortBy,
      };
    default:
      return state;
  }
};

export default DataReducer;
