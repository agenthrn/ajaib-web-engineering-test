import axios from "axios";
import React, { useReducer } from "react";
// Bring the context
import DataContext from "./DataContext";
// Bring the reducer
import DataReducer from "./DataReducer";

const DataState = ({ children }) => {
  const baseUrl = "https://randomuser.me/api";
  //set init state
  const initialState = {
    isLoading: false,
    selectedGender: "all",
    currentPage: 1,
    lastPage: 10,
    users: [],
    keyword: "",
    selectedSortOrder: "",
    selectedSortBy: "",
  };

  // Dispatch the reducer
  const [state, dispatch] = useReducer(DataReducer, initialState);

  //fetch api to get data
  const getData = async (page = 1, gender = "all") => {
    try {
      let params = { page: page, results: 10 };
      if (gender !== "all") params = Object.assign(params, { gender: gender });
      if (state.keyword !== "")
        params = Object.assign(params, { keyword: state.keyword });
      if (state.selectedSortOrder !== "")
        params = Object.assign(params, { sortOrder: state.selectedSortOrder });
      if (state.selectedSortBy !== "")
        params = Object.assign(params, { sortBy: state.selectedSortBy });
      dispatch({ type: "SET_LOADING", payload: true });
      const response = await axios.get(`${baseUrl}`, { params: params });
      //dispatch
      dispatch({
        type: "SET_DATA",
        payload: { response: response.data, selectedGender: gender },
      });
    } catch (e) {}
  };

  //set keyword
  const setKeyword = (keyword) => {
    dispatch({
      type: "SET_KEYWORD",
      payload: keyword,
    });
  };

  //set sort
  const setSort = (sortOrder, sortBy) => {
    dispatch({
      type: "SET_SORT",
      payload: { sortOrder, sortBy },
    });
    getData(state.currentPage, state.gender);
  };

  //reset data
  const resetData = () => {
    dispatch({
      type: "RESET_DATA",
      payload: initialState,
    });
    getData();
  };

  return (
    <DataContext.Provider
      value={{
        ...state,
        getData,
        resetData,
        setKeyword,
        setSort,
      }}
    >
      {children}
    </DataContext.Provider>
  );
};

export default DataState;
