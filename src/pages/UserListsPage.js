import React, { useContext, useEffect, useMemo } from "react";
import {
  ChakraProvider,
  Text,
  Container,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableContainer,
  Stack,
  Button,
  Select,
  InputGroup,
  Input,
  InputRightElement,
  Progress,
  chakra,
} from "@chakra-ui/react";
import { TriangleDownIcon, TriangleUpIcon } from "@chakra-ui/icons";
import { useTable, useSortBy } from "react-table";
import DataContext from "../context/DataContext";
import Pagination from "../context/Pagination";

const UserListsPage = () => {
  const {
    getData,
    resetData,
    isLoading,
    users,
    selectedGender,
    currentPage,
    keyword,
    setKeyword,
    setSort,
    selectedSortBy,
    selectedSortOrder,
  } = useContext(DataContext);
  const genders = ["all", "male", "female"];

  //declare column data of table
  const columns = useMemo(
    () => [
      {
        Header: "Username",
        accessor: "login.username",
      },
      {
        Header: "Name",
        accessor: "name.first",
      },
      {
        Header: "Gender",
        accessor: "gender",
      },
      {
        Header: "Register Date",
        accessor: "registered.date",
      },
    ],
    []
  );

  //set table data from data context
  const data = useMemo(() => [...users], [users]);

  const {
    state: { sortBy },
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({ columns, data, manualSortBy: true }, useSortBy);

  //get data from data context on page first load or state update
  useEffect(() => {
    getData();
  }, [selectedSortBy, selectedSortOrder]); // eslint-disable-line react-hooks/exhaustive-deps

  //catch sort by action from table
  useEffect(() => {
    sortBy.length > 0 &&
      setSort(sortBy[0].desc ? "descend" : "ascend", sortBy[0].id);
  }, [sortBy]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <ChakraProvider>
      <Container maxW="1100px">
        <Text fontSize="4xl">Random User Data</Text>
        <Stack maxW="800px" direction={["column", "row"]}>
          <InputGroup size="md">
            <Input
              pr="4.5rem"
              type="text"
              value={keyword}
              onChange={(e) => setKeyword(e.target.value)}
            />
            <InputRightElement width="4.5rem">
              <Button
                h="1.75rem"
                size="sm"
                onClick={() => getData(currentPage, selectedGender, keyword)}
              >
                Search
              </Button>
            </InputRightElement>
          </InputGroup>
          <Select
            onChange={(e) => getData(currentPage, e.target.value)}
            maxW="200px"
            value={selectedGender}
          >
            {genders.map((gender) => (
              <option key={gender} value={gender}>
                {gender}
              </option>
            ))}
          </Select>
          <Button onClick={() => resetData()}>Reset Data</Button>
        </Stack>

        <TableContainer>
          <Table {...getTableProps()}>
            <Thead>
              {headerGroups.map((headerGroup) => (
                <Tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <Th
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                      {column.render("Header")}
                      <chakra.span pl="4">
                        {column.isSorted ? (
                          column.isSortedDesc ? (
                            <TriangleDownIcon aria-label="sorted descending" />
                          ) : (
                            <TriangleUpIcon aria-label="sorted ascending" />
                          )
                        ) : null}
                      </chakra.span>
                    </Th>
                  ))}
                </Tr>
              ))}
            </Thead>
            <Tbody {...getTableBodyProps()}>
              {rows.map((row) => {
                prepareRow(row);
                return (
                  <Tr {...row.getRowProps()}>
                    {row.cells.map((cell) => (
                      <Td
                        {...cell.getCellProps()}
                        isNumeric={cell.column.isNumeric}
                      >
                        {cell.render("Cell")}
                      </Td>
                    ))}
                  </Tr>
                );
              })}
            </Tbody>
          </Table>
        </TableContainer>
        {isLoading && <Progress size="xs" isIndeterminate />}
        <Pagination />
      </Container>
    </ChakraProvider>
  );
};

export default UserListsPage;
